# Basic Usage

> git clone https://gitlab.com/dlystyr/vagrant-nodedev-env.git

To bring the virtual machine up
> vagrant up

To stop the virtual machine
> vagrant halt

SSH into machine using vagrant, you can also use port 2222 with your usual ssh application
> vagrant ssh

# Directories

> applications - _All Project Files_

> sites-available - _Nginx configuration files_

# Forwarded Ports

### Webserver
* guest: 80, host: 3480
* guest: 443, host: 3443

### MySQL
* guest: 3306, host: 3306

### SSH
* guest: 22, host: 2222