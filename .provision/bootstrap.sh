#!/bin/bash
echo "Setting up environment"

echo "Installing basic software and tools"
sudo apt-get update > /dev/null 2>&1
sudo apt-get install -y openssh-server  > /dev/null 2>&1
sudo apt-get install -y git curl  > /dev/null 2>&1

echo "Installing mysql"
sudo apt-get install -y debconf-utils > /dev/null 2>&1
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password oblivion"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password oblivion"

sudo apt-get install -y mysql-server > /dev/null 2>&1

echo "Installing nginx"
sudo apt-get install -y nginx > /dev/null 2>&1
sudo systemctl enable nginx.service > /dev/null 2>&1

echo "Installing php-fpm"
sudo apt-get install -y php-fpm php-mbstring php-gettext > /dev/null 2>&1
sudo apt install -y php-dev libmcrypt-dev php-pear > /dev/null 2>&1
sudo pecl channel-update pecl.php.net > /dev/null 2>&1
sudo pecl install mcrypt-1.0.1 > /dev/null 2>&1
echo "extension=mcrypt.so" | sudo tee --append /etc/php/7.2/cli/php.ini > /dev/null 2>&1

echo "Installing phpmyadmin"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/internal/skip-preseed boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean false"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password oblivion"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password oblivion"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password oblivion"
sudo apt-get install -y phpmyadmin > /dev/null 2>&1

echo "Installing nodejs"
sudo curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash - > /dev/null 2>&1
sudo apt-get install -y nodejs > /dev/null 2>&1

echo "Installing yarn"
sudo curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - > /dev/null 2>&1
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee --append /etc/apt/sources.list.d/yarn.list > /dev/null 2>&1
sudo apt-get update > /dev/null 2>&1
sudo apt-get install -y yarn > /dev/null 2>&1

echo "Creating user: nodedev"
sudo adduser --disabled-password --gecos "" nodedev
sudo usermod -aG sudo nodedev
echo nodedev:vagrant | sudo chpasswd

sudo mkdir -p /home/nodedev/applications
sudo chown nodedev:nodedev /home/nodedev/applications
sudo chmod 755 /home/nodedev/applications

echo "Finishing up"
sudo ln -s /usr/share/phpmyadmin /var/www/html/phpmyadmin

sudo systemctl restart nginx > /dev/null 2>&1
sudo systemctl restart mysql > /dev/null 2>&1
sudo systemctl restart php7.2-fpm > /dev/null 2>&1

echo "+---------------------------------------------------------+"
echo "|                      S U C C E S S                      |"
echo "+---------------------------------------------------------+"
echo "|                    You're good to go!                   |"
echo "|                                                         |"
echo "|          You can SSH in with nodedev / vagrant          |"
echo "|                                                         |"
echo "|        You can login to MySQL with root / oblivion      |"
echo "+---------------------------------------------------------+"
